# formCreate

#### 介绍
formCreate 动态生成表单且绑定事件 - layui 第三方扩展组件

#### 组件截图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/112238_3f5dee1a_631785.png "微信截图_20210323112200.png")


#### 安装教程

1.  下载后复制formCreate目录所有文件到LayUi组件目录即可

#### 示例代码
    >         <script src="../../../layui/layui.js"></script>
        <script>
            layui.config({
                base: '../../../layui_exts/' //配置 layui 第三方扩展组件存放的基础目录
            }).extend({
                formCreate: 'formCreate/formCreate'
            }).use(['formCreate', 'form'], function () {
                var formCreate = layui.formCreate, form = layui.form;
                var data = [
                    {
                        "id": 1,
                        "name": "system_action_log_open",
                        "title": "系统日志",
                        "type": "radio",
                        "params": "0=关闭\n1=开启",
                        "rules": "",
                        "value": "1",
                        "descs": "开启记录系统操作日志"
                    },
                    {
                        "id": 2,
                        "name": "admin_name",
                        "title": "系统名称",
                        "type": "text",
                        "params": "",
                        "rules": "required",
                        "value": "",
                        "descs": "系统后台的名称"
                    },
                    {
                        "id": 3,
                        "name": "file",
                        "title": "后台地址",
                        "type": "upload",
                        "params": "accept=file",
                        "rules": "required",
                        "value": "http://www.admin.com/upload/admin/20210305/6e2c5521f0e7548a45f260a09503973e.jpg",
                        "descs": "上传单个文件"
                    },
                    {
                        "id": 4,
                        "name": "image",
                        "title": "上传",
                        "type": "upload",
                        "params": "",
                        "rules": "required",
                        "value": "",
                        "descs": "上传单个图片"
                    }
                ];
                //执行示例
                formCreate.render({
                    elem: '#LAY-system-config-setting-form-create',
                    data: data,
                    callback: function (dom) {
                        form.render(null, 'LAY-system-config-setting-form');
                        //监听提交
                        form.on('submit(LAY-system-config-setting-submit)', function (data) {
                            var field = data.field; //获取提交的字段
                            console.log(field);
                        });
                    }
                });
            });
        </script>

#### 使用说明

1.  在formCreate.js配置一下默认的上传路径以及上传时的头部参数
